# LIS4369 - Extensible Enterprise Solutions

## Ryan Alger

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install .NET Core
	- Create hwapp Application
	- Create aspnetcoreapp Application
	- Display Screenshots of Installations
	- Create Bitbucket Repo
	- Complete Bitbucket Tutorials
	- Provide Git Command Descriptions
	
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Reverse Engineer the Calculator Application
    - Prevent Division by Zero
    - Print Assignment Requirements and Time Stamp

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Reverse Engineer the Future Value Calculator
    - Print Assignment Requirements and Time Stamp
    - Create the FutureValue Method
    - Perform Data Validation
	
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Person class
    - Create Student class
    - Allow user to press any key to return to command line
 
	
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create a new razor project
    - update the home page
    - update contact page
    - update about page
	
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Reverse engineer the Room Size Calculator using Classes application
    - Perform Data Validation
    - Create default constructor and parameterized constructor for Room object
    - Create getters and setters for each data member + getArea and getVolume
    - Round double values to two decimal places
	
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Complete the LINQ Tutorial Program
    - Use LINQ to search for a person by their last name
    - Use LINQ to search for a person by their age AND occupation