# LIS4369 - Extensible Enterprise Solutions

## Ryan Alger

### Assignment 5 Requirements:

*Three Parts:*

1. Create a new razor project
2. update the home page
3. update contact page
4. update about page
 
#### Assignment Screenshots:

*Screenshot of the Home Page:*

![Future Value Calculator](img/home.png)

Screenshot of the Contact Page:*

![Future Value Calculator](img/contact.png)

Screenshot of the About Page:*

![Future Value Calculator](img/about.png)
