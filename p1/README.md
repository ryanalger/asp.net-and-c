# LIS4369 - Extensible Enterprise Solutions

## Ryan Alger

### Project 1 Requirements:

*Five Parts:*

1. Reverse engineer the Room Size Calculator using Classes application
2. Perform Data Validation
3. Create default constructor and parameterized constructor for Room object
4. Create getters and setters for each data member + getArea and getVolume
5. Round double values to two decimal places
 

#### Assignment Screenshots:

*Screenshot of Room Size Calculator using Classes:*

![Future Value Calculator](img/p1.png)

