using System;

namespace RoomSizeCalc
{ 
   public class Room
    {
        //create Room data members
        private string type; //room type
        private double length; //room length
        private double width; //room width
        private double height; //room height

        //constructors are a special type of method, they are neither void nor value returning
        //default constructor to initialize data members
        public Room()
        {
            type = "Default";
            length = 10.0;
            width = 10.0;
            height = 10.0;

            //this keyword refers to the current instance of the object
            Console.WriteLine("Creating " + this.type + " room object from default constructor (accepts no arguments):");
        }

        //parameterized constructor to initialize data members based on user input
        public Room(string t = "", double l = 0.0, double w = 0.0, double h = 0.0)
        {
            type = t;
            length = l;
            width = w;
            height = h;

            Console.WriteLine("\nCreating " + this.type + " room object from parameterized constructor (accepts four arguments):");
        }

        //setter method for TYPE
        public void setType(string t = "default")
        {
            type = t;
        }

        //getter method for TYPE
        public string getType()
        {
            return type;
        }
       
        //setter method for LENGTH
        public void setLength(double l = 0.0)
        {
            length = l;
        }

        //getter method for LENGTH
        public double getLength()
        {
            return length;
        }

        //setter method for WIDTH
        public void setWidth(double w = 0.0)
        {
            width = w;
        }

        //getter method for WIDTH
        public double getWidth()
        {
            return width;
        }

        //setter method for HEIGHT
        public void setHeight(double h = 0.0)
        {
            height = h;
        }

        //getter method for HEIGHT
        public double getHeight()
        {
            return height;
        }

        //getter for AREA
        public double getArea()
        {
            return (length * width);
        }
        
        //getter for VOLUME
        public double getVolume()
        {
            return (length * width * height);
        }
    }
}