﻿using System;

namespace RoomSizeCalc
{ 
    class Program
    {
        //validates that entered data is a double, if not loops until a double is entered and returns said double
        public static double ValidDouble(string prompt) 
          {
            double validNumber = 0.0;
            Console.Write(prompt + ": ");
            while (!Double.TryParse(Console.ReadLine(), out validNumber))
            {
                Console.WriteLine("\n" + prompt + " must be numeric.");
                Console.Write(prompt + ": ");
            }
            return validNumber;
          }

        static void Main(string[] args)
        {
//border string for formatting
string border = "=======================================================================================";

//verbatim string literal
string requirements =
@"
P1: Room Size Calculator using Classes
Author: Ryan Alger
Progam Requirements:
  1) Create the Room class
  2) Create the following fields (aka properties or data members):
    a. private string type   -> room type
    b. private double length -> room length
    c. private double width  -> room width
    d. private double height -> room height
  3) Create two constructors:
    a. Default constructor
    b. Parameterizedconstructor that accepts four arguments (four fields above)
  4) Create the following mutator (aka setter) methods:
    a. setType
    b. setLength
    c. setWidth
    d. setHeight
  5) Create the following accessor (aka getter) methods:
    a. getType
    b. getLength
    c. getWidth
    d. getHeight
    e. getArea
    f. getVolume
  6) Must include the following functionality:
    a. Display room size calculations in feet
    b. Must incude data validation
    c. Round to two decimal places
  7) Allow user to press any key to return to the command line.
";
            Console.WriteLine(border);
            Console.WriteLine(requirements);
            Console.WriteLine(border);

            DateTime localDate = DateTime.Now;
            Console.WriteLine("\nNow: " + localDate.ToString("ddd") + ", " + localDate + "\n");

            //create default room0
            Room room0 = new Room();

            //type
            Console.Write("Room Type: ");
            Console.WriteLine(room0.getType());

            //length
            Console.WriteLine("Room Length: " + room0.getLength().ToString());

            //width
            Console.WriteLine("Room Width: " + room0.getWidth().ToString());

            //height
            Console.WriteLine("Room Height: " + room0.getHeight().ToString());

            //area
            Console.WriteLine("Room Area: " + room0.getArea().ToString("F2") + " sq ft");
           
            //volume in cubic feet
            Console.WriteLine("Room Volume: " + room0.getVolume().ToString("F2") + " cu ft");

            //volume in cubic yards
            Console.WriteLine("Room Volume: " + (room0.getVolume() / 27).ToString("F2") + " cu yd");
            
            Console.WriteLine("\nModify Default room object's data member values:");
            Console.WriteLine("Use setter methods");
        
            //initialize variables for user input
            string rType = "";
            double rLength = 0.0;
            double rWidth = 0.0;
            double rHeight = 0.0;

            //modify default room
            //capture user input for Room Type
            Console.Write("Room Type: ");
            rType = Console.ReadLine();
            room0.setType(rType);   //set room0's type to user input

            //capture valid user input for Room LENGTH
            string prompt = "Room Length"; //used for ValidDouble method for use friendliness
            rLength = ValidDouble(prompt);
            room0.setLength(rLength);

            //capture and set valid user input for Room WIDTH
            prompt = "Room Width";
            rWidth = ValidDouble(prompt);
            room0.setWidth(rWidth);

            //capture and set valid user input for Room HEIGHT
            prompt = "Room Height";
            rHeight = ValidDouble(prompt);
            room0.setHeight(rHeight);

            Console.WriteLine("\nDisplay Default room object's new data member values:");
            Console.WriteLine("Use getter methods");

            //get new type
            Console.WriteLine("Room Type: " + room0.getType());

            //get new length
            Console.WriteLine("Room Length: " + room0.getLength().ToString());

            //get new width
            Console.WriteLine("Room Width: " + room0.getWidth().ToString());

            //get new height
            Console.WriteLine("Room Height: " + room0.getHeight().ToString());

            //get new area
            Console.WriteLine("Room Area: " + room0.getArea().ToString("F2"));

            //get new volume
            Console.WriteLine("Room Volume: " + room0.getVolume().ToString("F2") + " cu ft");
            
            //get volume in cubic yards
            Console.WriteLine("Room Volume: " + (room0.getVolume() / 27 ).ToString("F2") + " cu yd");
            
            //reinitialize variables for user input
            rType = "";
            prompt = "";
            rLength = 0.0;
            rWidth = 0.0;
            rHeight = 0.0;
            //volumeYd = 0.0;

            Console.WriteLine("\nCall parameterized constructor (accepts four arguments):");

            //get type from user
            Console.Write("Room Type: ");
            rType = Console.ReadLine();

            //get valid length from user
            prompt = "Room Length";
            rLength = ValidDouble(prompt);

            //get valid width from user
            prompt = "Room Width";
            rWidth = ValidDouble(prompt);

            //get valid height from user
            prompt = "Room Height";
            rHeight = ValidDouble(prompt);

            //create new room object using validated user input
            Room room1 = new Room(rType, rLength, rWidth, rHeight);

            //get type
            Console.WriteLine("Room Type: " + room1.getType());

            //get length
            Console.WriteLine("Room Length: " + room1.getLength().ToString());

            //get width
            Console.WriteLine("Room Width: " + room1.getWidth().ToString());

            //get height
            Console.WriteLine("Room Height: " + room1.getHeight().ToString());

            //get area
            Console.WriteLine("Room Area: " + room1.getArea().ToString("F2") + " sq ft");

            //get volume
            Console.WriteLine("Room Volume: " + room1.getVolume().ToString("F2") + " cu ft");

            //get volume in cubic yards
            Console.WriteLine("Room Volume: " + (room1.getVolume() / 27).ToString("F2") + " cu yd");

            Console.WriteLine("\nPress any key to exit...");
            Console.ReadKey();
        }
    }
}