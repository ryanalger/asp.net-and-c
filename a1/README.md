# LIS4369 - Extensible Enterprise Solutions

## Ryan Alger

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Git Commands 
4. Bitbucket Tutorial Repo Links

#### Git Commands w/ Short Descriptions:

1. git init - Create an empty git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and merge with another repository or local branch
7. git mv - Move or rename a file, directory or symlink

#### Assignment Screenshots:

*Screenshot of hwapp Application:*

![hwapp Application](img/hwapp.png)

[*Screenshot of aspnetcoreapp Application:*](http://localhost:5000/About "asp-net-core-app")

![aspnetcoreapp Application](img/aspnetcoreapp.png)

#### Bitbucket Tutorial Links:

*Tutorial - Station Locations:*
[A1: Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ryanalger/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial -  Request to update a teammate's repository:*
[A1: My Team Quotes Tutorial Link](https://bitbucket.org/ryanalger/myteamquotes/ "My Team Quotes Tutorial")