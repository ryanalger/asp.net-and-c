﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    public class Program
    {
        public static void Main()
        {
            string reqs =
@"+-------------------------------------------------------------------------+
| Using LINQ - Language Integrated Query                                  |
|   Author: Ryan Alger                                                    |
|-------------------------------------------------------------------------|
| Part 1: LINQ Tutorial                                                   |
| Part 2: Required Program                                                |
|   1. Prompt user for last name, return full name, occupation and age.   |
|   2. Prompt user for age and occupation. Return full name.              |
|   3. Allow user to press any key to return to command line.             |
+-------------------------------------------------------------------------+
";
            Console.WriteLine(reqs);
            var people = GenerateListOfPeople();
            
            //***FINDING ITEMS IN COLLECTION***
            Console.WriteLine("PART 1: \n*** Finding Items in Collections ***");
            
            //where
            Console.WriteLine("\nWhere:");
            var peopleOverTheAgeOf30 = people.Where(x => x.Age > 30);
            foreach (var person in peopleOverTheAgeOf30)
            {
                Console.WriteLine(person.FirstName);
            }

            //skip
            Console.WriteLine("\nSkip:");
            IEnumerable<Person> afterTwo = people.Skip(2);
            foreach (var person in afterTwo)
            {
                Console.WriteLine(person.FirstName);
            }

            //take
            Console.WriteLine("\nTake:");
            IEnumerable<Person> takeTwo = people.Take(2);
            foreach (var person in takeTwo)
            {
                Console.WriteLine(person.FirstName);
            }

            Continue();
        
            //***CHANGING EACH ITEM IN COLLECTIONS***
            Console.WriteLine("\n*** Changing Each Item in Collections ***");

            //select
            Console.WriteLine("\nSelect:");
            IEnumerable<string> allFirstNames = people.Select(x => x.FirstName);
            foreach (var firstName in allFirstNames)
            {
                Console.WriteLine(firstName);
            }

            //fullname class and objects
            Console.WriteLine("\nFullname class and Objects:");
            IEnumerable<FullName> allFullNames = people.Select(x => new FullName { First = x.FirstName, Last = x.LastName });
            foreach (var fullName in allFullNames)
            {
                Console.WriteLine($"{fullName.Last}, {fullName.First}");
            }

            Continue();
            
            //***FINDING ONE ITEM IN COLLECTIONS***
            Console.WriteLine("\n*** Finding One Item in Collections ***");

            //first or default
            Console.WriteLine("\nFirstOrDefault:");
            Person firstOrDefault = people.FirstOrDefault();
            Console.WriteLine(firstOrDefault.FirstName);

            //first or default as filter
            Console.WriteLine("\nFirstOrDefault as Filter:");
            var firstThirtyYearOld1 = people.FirstOrDefault(x => x.Age == 30);
            var firstThirtyYearOld2 = people.Where(x => x.Age == 30).FirstOrDefault();
            Console.WriteLine(firstThirtyYearOld1.FirstName); 
            Console.WriteLine(firstThirtyYearOld2.FirstName);

            //how OrDefault
            Console.WriteLine("\nHow OrDefault Works:");

            List<Person> emptyList = new List<Person>();
            Person willBeNull = emptyList.FirstOrDefault();

            List<Person> people2 = GenerateListOfPeople();
            Person willAlsoBeNull = people2.FirstOrDefault(x => x.FirstName == "John"); 

            Console.WriteLine(willBeNull == null);
            Console.WriteLine(willAlsoBeNull == null);

            //LastOrDefault as filter
            Console.WriteLine("\nLastOrDefault as Filter:");

            Person lastOrDefault = people.LastOrDefault();
            Console.WriteLine(lastOrDefault.FirstName);
            Person lastThirtyYearOld = people.LastOrDefault(x => x.Age == 30);
            Console.WriteLine(lastThirtyYearOld.FirstName);

            //SingleOrDefault as filter
            Console.WriteLine("\nSingleOrDefault as Filter:");
            Person single = people.SingleOrDefault(x => x.FirstName == "Eric"); 
            Console.WriteLine(single.FirstName);

            Continue();

            //***FINDING DATA ABOUT COLLECTIONS***
            Console.WriteLine("\n*** Finding Data About Collections ***");

            //count
            Console.WriteLine("\nCount():");
            int numberOfPeopleInList = people.Count();
            Console.WriteLine(numberOfPeopleInList);

            //count with predicate expression
            Console.WriteLine("\nCount() with Predicate Expression:");
            int peopleOverTwentyFive = people.Count(x => x.Age > 25);
            Console.WriteLine(peopleOverTwentyFive);

            //any
            Console.WriteLine("\nAny():");
            people = GenerateListOfPeople();
            emptyList = new List<Person>();

            bool thereArePeople = people.Any();
            Console.WriteLine(thereArePeople);
            bool thereAreNoPeople = emptyList.Any();
            Console.WriteLine(thereAreNoPeople);

            //all
            Console.WriteLine("\nAll():");
            bool allDevs = people.All(x => x.Occupation == "Dev");
            Console.WriteLine(allDevs);
            bool everyoneAtLeastTwentyFour = people.All(x => x.Age >= 24);
            Console.WriteLine(everyoneAtLeastTwentyFour);

            Continue();

            //***CONVERTING RESULTS TO COLLECTIONS***
            Console.WriteLine("\n*** Converting Results to Collections ***");

            //ToList()
            Console.WriteLine("\nToList():");
            List<Person> listOfDevs = people.Where(x => x.Occupation == "Dev").ToList();
            foreach (var person in listOfDevs)
            {
                Console.WriteLine(person.FirstName);
            }

            //ToArray()
            Console.WriteLine("\nToArray():");
            Person[] arrayOfDevs = people.Where(x => x.Occupation == "Dev").ToArray();
            foreach (var person in arrayOfDevs)
            {
                Console.WriteLine(person.FirstName);
            }


            //***REQUIRED PROGRAM***
            //search variables
            string s_lname = "";
            string s_job = "";  // aka occupation
            int s_age = 0;

            Console.WriteLine("\nPART2:");
            Console.WriteLine("*** REQUIRED PROGRAM *** \nNote: Searches are Case Sensitive.");
            
            //search lname
            Console.Write("\nPlease enter last name: ");
            s_lname = Console.ReadLine();

            var match = people.FirstOrDefault(x => x.LastName == s_lname);
            Console.Write("\nMatching Criteria: ");
            Console.Write($"{match.FirstName} {match.LastName} is a {match.Occupation}, and is {match.Age} years old.\n");
            
            //capture s_age
            Console.Write("\nPlease enter age: ");
            while(!int.TryParse(Console.ReadLine(), out s_age))
            {
                Console.Write("  Age must be an integer.\n  Enter Age: ");
            }

            //capture s_job
            Console.Write("Please enter occupation: ");
            s_job = Console.ReadLine();
            while(s_job != "Manager" && s_job != "Dev")
            {
                Console.Write("  Invalid Occupation\n  Please enter either Manager or Dev: ");
                s_job = Console.ReadLine();
            }
/*
            //create a list of all the people with s_age
            List<Person> s_ageList = people.Where(x => x.Age == s_age).ToList();
            List<Person> matches = s_ageList.Where(x => x.Occupation == s_job).ToList();    //create a sublist based on the previous list and filter for s_job

            foreach (var person in matches)
            {
                Console.WriteLine($"\nMatching Criteria: {person.FirstName} {person.LastName}");    //print all the matches
            }
 */
            //find a person with given age and occupation
            var matches = people.Where(x => x.Age == s_age && x.Occupation == s_job);
            foreach (var person in matches)
            {
                Console.WriteLine($"\nMatching Criteria: {person.FirstName} {person.LastName}");    //print all the matches
            }

            //done
            Console.WriteLine("\nPress any key to exit!");
            Console.ReadKey();
        }

        public static List<Person> GenerateListOfPeople()
        {
            var people = new List<Person>();

            people.Add(new Person { FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24 });
            people.Add(new Person { FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
            people.Add(new Person { FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 30 });
            people.Add(new Person { FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
            people.Add(new Person { FirstName = "Samantha", LastName = "Jones", Occupation = "Dev", Age = 24 });

            return people;
        }

        //generates continue prompts
        public static void Continue()
        {
            Console.Write("\nPress ENTER to continue...");
            Console.ReadKey();
            Console.WriteLine("");
        }
    }

    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Occupation { get; set; }
        public int Age { get; set; }
    }

    public class FullName
    {
        public string First { get; set; }
        public string Last { get; set; }
    }
}