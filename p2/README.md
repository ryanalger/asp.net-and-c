# LIS4369 - Extensible Enterprise Solutions

## Ryan Alger

### Project 2 Requirements:

*Three Parts:*

1. Complete the LINQ Tutorial Program
2. Use LINQ to search for a person by their last name
3. Use LINQ to search for a person by their age AND occupation

#### Assignment Screenshots:

*Screenshot 1 of LINQ Tutorial:*

![LINQ Tutorial](img/tutorial1.png)

*Screenshot 2 of LINQ Tutorial:*

![LINQ Tutorial](img/tutorial2.png)

*Screenshot of Required Program:*

![LINQ Tutorial](img/required.png)


