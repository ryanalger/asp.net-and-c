﻿using System;

namespace a2
{
    class Program
    {
        static void Main(string[] args)
        {
            //startup prompt w/ time stamp
            string border = "///////////////////////////////////////////////////";
            DateTime localDate = DateTime.Now;

            Console.WriteLine(border);
            Console.WriteLine("A2: Simple Calculator");
            Console.WriteLine("Note: Program does not perform data validation.");
            Console.WriteLine("Author: Ryan Alger");
            Console.WriteLine("Now: " + localDate);
            Console.WriteLine(border);

            //varibale initialization
            double num1 = 0;
            double num2 = 0;
            int mathOp = 0;

            //prompt and store num 1
            Console.Write("\nNum1: ");
            num1 = Convert.ToDouble(Console.ReadLine());
            
            //prompt and store num 2
            Console.Write("Num2: ");
            num2 = Convert.ToDouble(Console.ReadLine());

            //prompt and store mathematical operation selection
            Console.WriteLine("\n1 - Addition\n2 - Subtraction\n3 - Multiplication\n4 - Division");
            Console.Write("\nChoose a mathematical operation: ");
            mathOp = Convert.ToInt32(Console.ReadLine());

            //validate proper mathematical operation
            while (mathOp < 1  || mathOp > 4)
            {
                Console.WriteLine("\nAn incorrect mathematical operation was entered.\nPlease enter a valid number.");
                Console.Write("\nChoose a mathematical operation: ");
                mathOp = Convert.ToInt32(Console.ReadLine());
            } 
    
            //determine which mathematical operation to use
            if (mathOp == 1)        //addition
            {
                Console.WriteLine("\n*** Result of Addition Operation: ***\n" + (num1+num2));
            }
            else if (mathOp == 2)    //subtraction
            {
                Console.WriteLine("\n*** Result of Subtraction Operation: ***\n" + (num1 - num2));
            }
            else if (mathOp == 3)   //multipliction
            {
                Console.WriteLine("\n*** Result of Multiplication Operation: ***\n" + (num1 * num2));
            }
            else if (mathOp == 4)   //division
            {
                //prevent division by zero
                while (num2 == 0)
                {
                    Console.WriteLine("\nError: Cannot divide by zero!");
                    Console.Write("Please enter a new value for the second number: ");
                    num2 = Convert.ToDouble(Console.ReadLine());
                }

                Console.WriteLine("\n*** Result of Division Operation: ***\n" + (num1 / num2));           
            }
            
            //press any key to exit
            Console.WriteLine("\nPress any key to exit...");
            Console.ReadKey();
        }
    }
}