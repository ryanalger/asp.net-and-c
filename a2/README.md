# LIS4369 - Extensible Enterprise Solutions

## Ryan Alger

### Assignment 2 Requirements:

*Three Parts:*

1. Reverse Engineer the Calculator Application
2. Prevent Division by Zero
3. Print Assignment Requirements and Time Stamp

#### Assignment Screenshots:

*Screenshot of a Valid Operation:*

![hwapp Application](img/ValidOperation.png)

*Screenshot of an Invalid Operation:*

![hwapp Application](img/InvalidOperation.png)

*Screenshot of Preventing Division by Zero:*

![hwapp Application](img/DivisionByZero.png)