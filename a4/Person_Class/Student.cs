﻿using System;

namespace Person_Class
{ 
    //Student is a subclass of Person
    public class Student : Person
    {
        //create Person data members
        private string college;
        private string major;
        private double gpa;

        //constructors are a special type of method, they are neither void nor value returning
        //default constructor to initialize data members
        public Student()
        {
            college = "CCI";
            major = "IT";
            gpa = 4.0;
 
            Console.WriteLine("\nCreating derived student object from default constructor (accepts no arguments):");
           
            //this keyword refers to the current instance of the object
            //Console.WriteLine("\nCreating " + this.fname + " " + this.lname + "'s " + "person object from default constructor "
                //+ "(accepts no arguments):");
        }

        //call parameterized consturctor and explicitly call base class constructor
        public Student(string f = "", string l = "", int a = 0, string c = "", string m = "", double g = 4.0) : base(f, l, a)
        {
            college = c;
            major = m;
            gpa = g;

            Console.WriteLine("\nCreating derived student object from default constructor (accepts 4 arguments):");
        }

       public string GetFullName()
       {
           return GetFname() + " " + GetLname();
       }

       public override string GetObjectInfo()
       {
            return base.GetObjectInfo() + " in the college of " + this.college + ", majoirng in " + this.major 
                + ", and has a " + this.gpa + " GPA.";
       }
    }
}