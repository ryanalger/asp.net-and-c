﻿using System;

namespace Person_Class
{
    class Program
    {
        static void Main(string[] args)
        {
string reqs =
@"/////////////////////////////
1) Create Person Class (must be stored in discrete file apart from Main() method)
  
  A. Create 3 protected data members:
    1. fname
    2. lname
    3. age

  B. Create 3 setter/mutator methods:
    1. SetFname
    2. SetLname
    3. SetAge

  C. Create 4 accessor/getter methods:
    1. GetFname
    2. GetLname
    3. GetAge
    4. GetObjectInfo(virtual method (returns string): allows derived class to override base class method)

  D.  Create 2 consructors
    1. default constructor (accepts no arguments)
    2. parameterized constructor (accepts 3 arguments)

2) Create Student class (must be stored in discrete file apart from Main() method)

  A. Create 3 protected data members:
    1. college
    2. major
    3. gpa

  B. Create 3 accessor/getter methods:
    1. GetName
    2. GetFullName
    3. GetObjectInfo(Demonstrates polymorphism)

  C. Create 2 consructors
    1. default constructor (accepts no arguments)
    2. parameterized constructor (accepts 6 arguments)

  D. Instantiate two student objects
    1. one from default constructor
    2. one from parameterized constructor
/////////////////////////////
";
            //print current date & time
            Console.WriteLine("\nNow: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));

            Console.WriteLine(reqs);
            //create new Person object named 'person1' using default constructor (no parameters) 
            Person person1 = new Person();

            //use the getter methods to display person1's data members
            Console.Write("  First Name: ");
            Console.WriteLine(person1.GetFname());

            Console.Write("  Last Name: ");
            Console.WriteLine(person1.GetLname());

            Console.Write("  Age: ");
            Console.WriteLine(person1.GetAge());

            //getting user input to modify person1's data members
            Console.WriteLine("\nModify person object's data memeber values created from default constructor:" 
                + "\nUse setter/getter methods:");

            Console.Write("  First Name: ");
            String p_fname = Console.ReadLine();

            Console.Write("  Last Name: ");
            String p_lname = Console.ReadLine();

            int p_age = 0;

            Console.Write("  Age: ");

            while(!int.TryParse(Console.ReadLine(), out p_age))
            {
                Console.Write("  Age must be an integer.\n  Enter Age: ");
            } 

            //setting the new values for person1's data memebrs
            person1.SetFname(p_fname);
            person1.SetLname(p_lname);
            person1.SetAge(p_age);

            //display the new data members for person1
            Console.WriteLine("\nDisplay object's new data members:");
            
            Console.Write("  First Name: ");
            Console.WriteLine(person1.GetFname());

            Console.Write("  Last Name: ");
            Console.WriteLine(person1.GetLname());

            Console.Write("  Age: ");
            Console.WriteLine(person1.GetAge());
            
            //call the parameterized constructor with 3 arguments
            Console.WriteLine("\nCall parameterized base constructor (accepts 3 args): ");

            Console.Write("  First Name: ");
            p_fname = Console.ReadLine();

            Console.Write("  Last Name: ");
            p_lname = Console.ReadLine();

            Console.Write("  Age: ");

            while(!int.TryParse(Console.ReadLine(), out p_age))
            {
                Console.Write("  Age must be an integer.\n  Enter Age: ");
            }

            //create new Person object named 'person2' using parameterized constructor
            Person person2 = new Person(p_fname, p_lname, p_age);

            //display person2's data members
            Console.Write("  First Name: ");
            Console.WriteLine(person2.GetFname());

            Console.Write("  Last Name: ");
            Console.WriteLine(person2.GetLname());

            Console.Write("  Age: ");
            Console.WriteLine(person2.GetAge());

            //create a new Student object named student1
            Console.WriteLine("\nCall derived default constructor (inherits from base class):");
            Student student1 = new Student();

            Console.Write("  First Name: ");
            Console.WriteLine(student1.GetFname());

            Console.Write("  Last Name: ");
            Console.WriteLine(student1.GetLname());

            Console.Write("  Age: ");
            Console.WriteLine(student1.GetAge());

            Console.WriteLine("\nDemonstrating Polymorphism (new derived object):");
            Console.WriteLine("(Calling parameterized base class construtor explicitly.)\n");

            Console.Write("  First Name: ");
            String s_fname = Console.ReadLine();

            Console.Write("  Last Name: ");
            String s_lname = Console.ReadLine();

            int s_age = 0;

            Console.Write("  Age: ");
            while(!int.TryParse(Console.ReadLine(), out s_age))
            {
                Console.Write("  Age must be an integer.\n  Enter Age: ");
            } 

            Console.Write("  College: ");
            string s_college = Console.ReadLine();

            Console.Write("  Major: ");
            string s_major = Console.ReadLine();

            double s_gpa = 0.0;

            Console.Write("  GPA: ");
            while(!double.TryParse(Console.ReadLine(), out s_gpa))
            {
                Console.Write("  GPA must be an integer.\n  Enter GPA: ");
            } 

            Student student2 = new Student(s_fname, s_lname, s_age, s_college, s_major, s_gpa);

            Console.WriteLine("\nperson2 - GetObjectInfo (virtual):\n");
            Console.WriteLine(person2.GetObjectInfo());

            Console.WriteLine("\nstudent2 - GetObjectInfo (overridden):\n");
            Console.WriteLine(student2.GetObjectInfo());

            Console.WriteLine("\nPress any key to exit...");
            Console.ReadKey();
        }
    }
}
