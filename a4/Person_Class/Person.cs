using System;

namespace Person_Class
{ 
   public class Person
    {
        //create Person data members
        protected string fname;
        protected string lname;
        protected int age;

        //constructors are a special type of method, they are neither void nor value returning
        //default constructor to initialize data members
        public Person()
        {
            fname = "First name";
            lname = "Last name";
            age = 0;
 
            Console.WriteLine("\nCreating base person object from default constructor (accepts no arguments):");
           
            //this keyword refers to the current instance of the object
            //Console.WriteLine("\nCreating " + this.fname + " " + this.lname + "'s " + "person object from default constructor "
            //    + "(accepts no arguments):");
        }

        //parameterized constructor to initialize data members based on user input
        public Person(string f = "", string l = "", int a = 0)
        {
            fname = f;
            lname = l;
            age = a;

            Console.WriteLine("\nCreating base person object from parameterized constructor (accepts 3 arguments):");

            //Console.WriteLine("\nCreating " + this.fname + " " + this.lname + "'s " + "Person object from parameterized constructor" 
            //    + "(accepts 3 arguments):");
        }

        //setter method for fname
        public void SetFname(string f = "")
        {
            fname = f;
        }

        //getter method for fname
        public string GetFname()
        {
            return fname;
        }
       
        //setter method for lname
        public void SetLname(string l = "")
        {
            lname = l;
        }

        //getter method for lname
        public string GetLname()
        {
            return lname;
        }

        public void SetAge(int a = 0)
        {
            age = a;
        }

        public int GetAge()
        {
            return age;
        }

        //virtual keyword allows for child class method to override parent class method
        public virtual string GetObjectInfo()
        {
            return fname + " " +lname + " is " + age.ToString();
        }
    }
}