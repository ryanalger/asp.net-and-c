# LIS4369 - Extensible Enterprise Solutions

## Ryan Alger

### Assignment 4 Requirements:

*Three Parts:*

1. Create Person class
2. Create Student class
3. Allow user to press any key to return to command line
 
#### Assignment Screenshots:

*Screenshot of the program running:*

![Future Value Calculator](img/a4.png)

