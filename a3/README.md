# LIS4369 - Extensible Enterprise Solutions

## Ryan Alger

### Assignment 3 Requirements:

*Four Parts:*

1. Reverse Engineer the Future Value Calculator
2. Print Assignment Requirements and Time Stamp
3. Create the FutureValue Method
4. Perform Data Validation
 

#### Assignment Screenshots:

*Screenshot of the Future Value Calculator:*

![Future Value Calculator](img/future_value.png)

