﻿using System;

namespace FutureValueCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            //startup prompt
            string border = "----------------------------------------------------------------------------------------------------------------------------------------------";
            DateTime localDate = DateTime.Now;

            Console.WriteLine(border);
            Console.WriteLine("Program Requirements:");
            Console.WriteLine("A3: Future Value Calculator");
            Console.WriteLine("Author: Ryan Alger");
            Console.WriteLine("  1) Use intrinsic method to display date/time;");
            Console.WriteLine("  2) Research: What is future value? And it's formula;");
            Console.WriteLine("  3) Create FutureValue method using the following parameters: decimal presentValue, int numYears, "
                                    + "decimal yearlyInt, decimal monthlyDep;");
            Console.WriteLine("  4) Initialize suitable variable(s): use decimal data types for currency variables;");
            Console.WriteLine("  5) Perform data validation: prompt user until correct data is entered;");
            Console.WriteLine("  6) Display money in currency format;");
            Console.WriteLine("  7) Allow user to press any key to return back to command line.");
            Console.WriteLine(border);
            Console.WriteLine("\nNow: " + localDate.ToString("ddd") + ", " + localDate + "\n");

            //get the STARTING BALANCE as a string
            Console.Write("Starting Balance: ");
            string input = "";
            input = (Console.ReadLine());
            
            //determine if STARTING BALANCE can be cast to a decimal, loop until one is entereted
            decimal input_dec = 0m;                
            Decimal.TryParse(input, out input_dec);
            while (input_dec == 0)
            {
                Console.WriteLine("Starting balance must be numeric.");
                Console.Write("Starting Balance: ");
                input = (Console.ReadLine());
                Decimal.TryParse(input, out input_dec);
            }

            //set the STARTING BALANCE to the validated input
            decimal startingBalance = 0m;
            startingBalance = Convert.ToDecimal(input);

            //get the TERM as a string
            Console.Write("\nTerm(years): ");
            input = "";
            input = (Console.ReadLine());

            //determine if TERM is actually an int, loop until an int is entered
            int input_int = 0;
            Int32.TryParse(input, out input_int);
            while (input_int == 0)
            {
                Console.WriteLine("Term must be an integer.");
                Console.Write("Term (years): ");
                input = (Console.ReadLine());
                Int32.TryParse(input, out input_int);
            }

            //set the TERM to the validated input
            int term = 0;
            term = Convert.ToInt32(input);

            //get the INTEREST as a string
            Console.Write("\nInterest Rate: ");
            input = ""; 
            input = (Console.ReadLine());
            
            //determine if INTEREST rate is actually a decimal, loop until one is entereted
            input_dec = 0m;                
            Decimal.TryParse(input, out input_dec);
            while (input_dec == 0)
            {
                Console.WriteLine("Interest Rate must be numeric.");
                Console.Write("Interest Rate: ");
                input = (Console.ReadLine());
                Decimal.TryParse(input, out input_dec);
            }

            //set the INTEREST rate to the validated input
            decimal interestRate = 0m;
            interestRate = Convert.ToDecimal(input);
            interestRate /= 100.0m;     //set the interest rate to a percent

            //get the MONTHLY DEPOSIT
                Console.Write("\nDeposit (monthly): ");
                input = ""; 
                input = (Console.ReadLine());
            
            //determine if MONTHLY DEPOSIT is actually a decimal, loop until one is entereted
            input_dec = 0m;                
            Decimal.TryParse(input, out input_dec);
            while (input_dec == 0)
            {
                Console.WriteLine("Monthly Deposit must be numeric.");
                Console.Write("Deposit (monthly): ");
                input = (Console.ReadLine());
                Decimal.TryParse(input, out input_dec);
            }

            decimal monthly = 0m;
            monthly = Convert.ToDecimal(input);

            //call the FutureValue method
            decimal futureValue = FutureValue (startingBalance, term, interestRate, monthly);

            Console.WriteLine("\n*** Future Value: ***");
            //format futureValue to USD
            Console.WriteLine(futureValue.ToString("C2"));

            //wrap it up
            Console.Write("\nPress any key to exit... ");
            Console.ReadKey();
        }

        //calculates the future value
        public static decimal FutureValue (decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep)
        {
            //thank you to http://www.thecalculatorsite.com/articles/finance/compound-interest-formula.php?page=2 for saving my GPA!
            
            //logic for COMPOUND INTEREST
            int compoundPower = 0;   //the result of N * T in the compound interest formula
            int numCompounds = 12;          //number of compounds per year
            compoundPower = (numCompounds * numYears);      

            decimal baseNum = 0.0m;         //the result of what is in the parenthesis in the compound interest formula
            baseNum = (1 + yearlyInt / numCompounds);

            decimal result = DecimalPow(baseNum, compoundPower);    //raise baseNum to the power of compoundPower, return the resulting decimal

            decimal compoundValue = 0.0m;
            compoundValue = (result * presentValue);

            //logic for FUTURE VALUE OF A SERIES
            decimal fvOfSeries = 
                monthlyDep * ((result - 1) / (yearlyInt / numCompounds));//* (1 + yearlyInt / numCompounds); <- uncomment if compounding at start of numYears

            //compute the TOTAL value
            decimal total = 0.0m;
            total = (compoundValue + fvOfSeries);

            return total;
        }
        
        //raises decimal uno to the power of int dos
        public static decimal DecimalPow(decimal uno, int dos)
        {
            //very important this isn't set to 0!
            decimal result = 1.0m;

            for (int i = 0; i < dos; i++)
            {
                result = (result * uno);
                //Console.WriteLine(i + "  -> " + result);
            }
            return result;
        }
    }
}